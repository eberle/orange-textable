Orange Textable
===============

Orange Textable is an open-source add-on bringing advanced text-analytical
functionalities to the `Orange Canvas <http://orange.biolab.si/>`_ data mining
software package (itself open-source). It essentially enables users to build
data tables on the basis of text data, by means of a flexible and intuitive
interface. Look at the following `example
<http://orange-textable.readthedocs.org/en/latest/illustration.html>`_ to see
it in typical action.

Orange Textable offers in particular the following features:

* text data import from keyboard, files, or urls
* support for various encodings, including Unicode
* standard preprocessing and custom recoding (based on regular expressions)
* segmentation and annotation of various text units (letters, words, etc.)
* ability to extract and exploit XML-encoded annotations
* automatic, random, or arbitrary selection of unit subsets
* unit context examination using concordance and collocation tables
* calculation of frequency and complexity measures
* recoded text data and table export

The project's homepage is http://langtech.ch/textable

Documentation is hosted at: http://orange-textable.readthedocs.org/

Orange Textable was designed and implemented by `LangTech Sarl
<http://langtech.ch>`_ on behalf of the `department of language and
information sciences (SLI) <http://www.unil.ch/sli>`_ at the `University of
Lausanne <http://www.unil.ch>`_ (see `Credits
<http://orange-textable.readthedocs.org/en/latest/credits.html>`_ and
`How to cite Orange Textable
<http://orange-textable.readthedocs.org/en/latest/credits.html>`_).
