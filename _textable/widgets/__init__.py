#=============================================================================
# Module __init__.py, v0.02
# Copyright 2012-2015 LangTech Sarl (info@langtech.ch)
#=============================================================================
# This file is part of the Textable (v1.5) extension to Orange Canvas.
#
# Textable v1.5 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Textable v1.5 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Textable v1.5. If not, see <http://www.gnu.org/licenses/>.
#=============================================================================

"""
Textable
========

"""

NAME = "Textable"

DESCRIPTION = """Add-on for text analysis"""

LONG_DESCRIPTION = """
This extension contains widgets for building data tables based on
heterogeneous text sources, using such operations as segmentation and
annotation.

"""

ICON = "icons/Category-Textable.png"

BACKGROUND = "#90c0ed"

